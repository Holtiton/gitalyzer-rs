use clap::ArgMatches;
use git2::{Commit, Repository};

use std::collections::HashMap;

use crate::Analysis;
use crate::get_diff;

pub struct Coupling {
    min_coupling_level: usize,
    coupling_map: HashMap<(String, String), usize>,
    file_map: HashMap<String, usize>,
}

impl Coupling {
    pub fn new(args: &ArgMatches) -> Self {
        let min_coupling_level = match args.value_of("coupling-level") {
            Some(level_str) => match level_str.parse() {
                Ok(level) => level,
                Err(err) => {
                    eprintln!("Failed to parse coupling-level: {}", err);
                    std::process::exit(1);
                }
            },
            None => 0,
        };

        let module_map = match args.value_of("module-file") {
            Some(file) => parse_module_file(file),
            None => None,
        };

        Self {
            min_coupling_level,
            coupling_map: HashMap::new(),
            file_map: HashMap::new(),
        }
    }
}

impl Analysis for Coupling {
    fn run(&mut self, repo: &Repository, commit: &Commit) {
        let mut files_changed = Vec::new();
        let diff = get_diff(commit, repo);

        for delta in diff.deltas() {
            let file = delta.new_file().path().unwrap().to_str().unwrap();
            files_changed.push(file);
            *self.file_map.entry(file.to_owned()).or_insert(0) += 1;
        }

        if files_changed.len() > 1 {
            let files_changed = &files_changed;
            let file_pairs = files_changed
                .iter().enumerate()
                .flat_map(move |t| std::iter::repeat(t.1).zip(files_changed.iter().skip(t.0 + 1)))
                .collect::<Vec<_>>();
            for pair in file_pairs {
                *self.coupling_map
                    .entry((pair.0.to_string(), pair.1.to_string()))
                    .or_insert(0) += 1
            }
        }
    }

    fn report(&self, limit: Option<usize>) {
        let mut print_count = 0;
        println!("file1; file2; changed_together; file1_changes; file1_coupling; file2_changes; file2_coupling");
        for (pair, coupling_level) in &self.coupling_map {
            if *coupling_level > self.min_coupling_level {
                if limit.is_some() && print_count >= limit.unwrap() {
                    break;
                }
                let file1_changes = *self.file_map.get(&pair.0).expect("File not found");
                let file2_changes = *self.file_map.get(&pair.1).expect("File not found");

                println!(
                    "{}; {}; {}; {}; {}; {}; {}",
                    pair.0, pair.1, coupling_level,
                    file1_changes, ((*coupling_level as f32 / file1_changes as f32) * 100f32) as u32,
                    file2_changes, ((*coupling_level as f32 / file2_changes as f32) * 100f32) as u32,
                );
                print_count += 1;
            }
        }
    }
}

fn parse_module_file(file_path: &str) -> Option<HashMap<String, Vec<String>>> {
    // use std::fs::File;
    // use std::io::BufRead;
    // use std::io::BufReader;

    let mut map: HashMap<String, Vec<String>> = HashMap::new();

    // let file = match File::open(file_path) {
    //     Ok(file) => file,
    //     Err(_) => {
    //         return None;
    //     }
    // };
    // let reader = BufReader::new(file);
    // for line in reader.lines() {
    //     let something: Vec<&str> = line.unwrap().split(":").collect();
    //     match map.get_mut(something[0]) {
    //         Some(value) => value.push(something[1].to_owned()),
    //         None => {
    //             map.insert(something[0].to_owned(), Vec::new());
    //         }
    //     }
    // }
    Some(map)
}
