use clap::ArgMatches;
use git2::{Commit, DiffStatsFormat, Repository};

use std::collections::HashMap;

use crate::{Analysis, get_stats};
use crate::get_diff;

pub struct CommitCount {
    files: HashMap<String, usize>,
}

impl CommitCount {
    pub fn new(args: &ArgMatches) -> Self {
        Self {
            files: HashMap::new(),
        }
    }
}

impl Analysis for CommitCount {
    fn run(&mut self, repo: &Repository, commit: &Commit) {
        let diff = get_diff(commit, repo);

        for delta in diff.deltas() {
            let file = delta.new_file().path().unwrap().to_str().unwrap();
            *self.files.entry(file.to_string()).or_insert(0) += 1;
        }
    }

    fn report(&self, limit: Option<usize>) {
        let mut sorted_counts: Vec<_> = self.files.iter().collect();
        sorted_counts.sort_by(|a, b| b.1.cmp(a.1));
        if limit.is_some() {
            for (file, count) in sorted_counts.iter().take(limit.unwrap()) {
                println!("{}; {}", file, count);
            }
        } else {
            for (file, count) in sorted_counts {
                println!("{}; {}", file, count);
            }
        }
    }
}

pub struct Churn {
    files: HashMap<String, (usize, usize, usize)>,
}

impl Churn {
    pub fn new(args: &ArgMatches) -> Self {
        Self {
            files: HashMap::new(),
        }
    }
}

impl Analysis for Churn {
    fn run(&mut self, repo: &Repository, commit: &Commit) {
        let diff = get_diff(commit, repo);

        let stats = diff.stats().unwrap();
        self.files = get_stats(&stats);
    }

    fn report(&self, limit: Option<usize>) {
        let mut sorted_counts: Vec<_> = self.files.iter().collect();
        sorted_counts.sort_by(|a, b| b.1.cmp(&a.1));
        if limit.is_some() {
            for (file, count) in sorted_counts.iter().take(limit.unwrap()) {
                println!("{}; {:?}", file, count);
            }
        } else {
            for (file, count) in sorted_counts {
                println!("{}; {:?}", file, count);
            }
        }
    }
}
