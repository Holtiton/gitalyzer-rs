use clap::ArgMatches;
use git2::{Commit, Repository};

use std::path::PathBuf;
use std::collections::HashMap;

use crate::{Analysis, get_diff, get_stats};

#[derive(Debug)]
struct ChangeStats {
    commit_count: usize,
    total: usize,
    added: usize,
    deleted: usize,
}

pub struct Ownership {
    file_ownership: HashMap<(String, String), ChangeStats>,
    path_only: bool,
}

impl Ownership {
   pub fn new(args: &ArgMatches) -> Self {
       let path_only = args.is_present("use base path as module");
       Self {
           file_ownership: HashMap::new(),
           path_only
       }
   }
}

impl Analysis for Ownership {
    fn run(&mut self, repo: &Repository, commit: &Commit) {
        let author = commit.author().name().unwrap().to_owned();
        let diff = get_diff(commit, repo);
        if diff.deltas().len() > 1000 {
            return
        };
        let stats = diff.stats().unwrap();
        for (mut file, stats) in get_stats(&stats) {
            if self.path_only {
                let mut p = PathBuf::from(file);
                p.pop();
                file = p.to_str().unwrap().to_string();
            }
            let stat = self.file_ownership.entry((file, author.clone())).or_insert(ChangeStats {
                commit_count: 1,
                total: stats.0,
                added: stats.1,
                deleted: stats.2,
            });
            stat.commit_count +=1;
            stat.total += stats.0;
            stat.added += stats.1;
            stat.deleted += stats.2;
        }
    }

    fn report(&self, limit: Option<usize>) {
        let mut sorted_changes: Vec<_> = self.file_ownership.iter().collect();
        println!("file; author; commits; total_changes; added; deleted");
        sorted_changes.sort_by(|a, b| b.0.cmp(&a.0));
        for (file, changes) in sorted_changes {
            println!("{}; {}; {}; {}; {}; {}", file.0, file.1, changes.commit_count, changes.total, changes.added, changes.deleted);
        }
    }
}
