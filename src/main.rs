use chrono;
use clap::{App, AppSettings, Arg, ArgMatches, SubCommand};
use git2::{Commit, Diff, Repository, DiffStats, DiffStatsFormat};

mod coupling;
mod commits;
mod ownership;

use coupling::{Coupling};
use commits::{CommitCount, Churn};
use ownership::{Ownership};
use std::collections::HashMap;
use std::hash::Hash;

trait Analysis {
    fn run(&mut self, repo: &Repository, commit: &Commit);
    fn report(&self, limit: Option<usize>);
}

fn main() {
    let args = App::new("Gitalyzer")
        .author("Janne Holtti <janne@holtiton.fi>")
        .about("Analyzes your Git history for things and stuff")
        .setting(AppSettings::ArgRequiredElseHelp)
        .arg(
            Arg::with_name("path")
                .short("r")
                .long("repository")
                .help("Path to Git repository")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("start date")
                .short("s")
                .long("since")
                .alias("from")
                .help("Start analysis at this date <YYYY-MM-DD>")
                .required(false)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("stop date")
                .short("u")
                .long("until")
                .alias("to")
                .help("Stop analysis at this date <YYYY-MM-DD>")
                .required(false)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("number of results")
                .short("l")
                .long("limit")
                .alias("top")
                .help("Limit results to the N first")
                .required(false)
                .takes_value(true),
        )
        .subcommand(
            SubCommand::with_name("coupling")
                .about("Calculate coupling of files in the git history")
                .arg(
                    Arg::with_name("coupling-level")
                        .short("l")
                        .long("coupling-level")
                        .help("Minimum coupling level to report")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("module-file")
                        .short("m")
                        .long("module-file")
                        .help("Module definitions file. <path> : <module-name> per line.")
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("commit-count")
                .about("Calculate commits per file in the git history")
        ).subcommand(
            SubCommand::with_name("churn")
                .about("Calculate churn")
        ).subcommand(
            SubCommand::with_name("ownership")
                .about("Ownership of files in the repository. Lists who has commited the most to a certain file.")
                .arg(
                    Arg::with_name("use base path as module")
                        .short("f")
                        .long("folders-only")
                ),
        )
        .get_matches();

    let repo_path = args.value_of("path").expect("Somehow there is no repo argument...");
    let repo = Repository::open(repo_path).expect("Unable to open repository");
    let since = get_arg_timestamp("start date", &args);
    let until = get_arg_timestamp("stop date", &args);

    let limit = if args.is_present("number of results") {
        Some(args.value_of("limit").unwrap().parse().unwrap())
    } else {
        None
    };

    let mut selected_analysis: Box<dyn Analysis> = match args.subcommand() {
        ("coupling", Some(args)) => { Box::new(Coupling::new(args)) },
        ("commit-count", Some(args)) => { Box::new(CommitCount::new(args)) },
        ("churn", Some(args)) => { Box::new(Churn::new(args)) },
        ("ownership", Some(args)) => { Box::new(Ownership::new(args)) },
        _ => {
            eprintln!("No subcommand found");
            std::process::exit(1);
        }
    };

    let mut walker = repo.revwalk().expect("Revwalker creation failed");
    walker.set_sorting(git2::Sort::REVERSE | git2::Sort::TIME);
    walker.simplify_first_parent();
    walker.push_head().expect("Push head failed");

    for commit_oid in walker {
        let commit_oid = commit_oid.unwrap();
        let commit = repo.find_commit(commit_oid).unwrap();

        if since.is_none() || (since.is_some() && commit.committer().when().seconds() > since.unwrap()) {
            selected_analysis.run(&repo, &commit);
        }

        if until.is_some() && commit.committer().when().seconds() > until.unwrap() {
            break;
        }
    }

    selected_analysis.report(limit);
}

fn get_arg_timestamp(arg_name: &str, matches: &ArgMatches) -> Option<i64> {
    if matches.is_present(arg_name) {
        match chrono::NaiveDate::parse_from_str(matches.value_of(arg_name).unwrap(), "%Y-%m-%d") {
            Ok(date) => Some(date.and_hms(0, 0, 0).timestamp()),
            Err(err) => {
                eprintln!("Date parsing failed for {}: {}", arg_name, err);
                std::process::exit(1);
            }
        }
    } else {
        // no user supplied date
        None
    }
}

fn get_diff<'r>(commit: &Commit, repo: &'r Repository) -> Diff<'r> {
    match commit.parents().len() {
        0 => repo
            .diff_tree_to_tree(
                None,
                commit.tree().ok().as_ref(),
                None).expect("Diff failed"),
        _ => repo
            .diff_tree_to_tree(
                commit.parent(0).expect("Commit parent failed").tree().ok().as_ref(),
                commit.tree().ok().as_ref(),
                None).expect("Diff failed"),
    }
}

fn get_stats(stats: &DiffStats) -> HashMap<String , (usize, usize, usize)> {
    // This is not a good API, as it requires a width parameter as to how long strings I'm willing to take
    // Dont know if I can fix it either so I have 200 now that should suffice for most things... maybe
    let stats = stats.to_buf(DiffStatsFormat::NUMBER, 200).expect("DiffStatsFormat failed");
    let stats = std::str::from_utf8(&*stats).unwrap();
    let mut files: HashMap<String, (usize, usize, usize)> = HashMap::new();
    for line in stats.lines() {
        let mut stat_line = line.split_whitespace();
        let added: usize = stat_line.next().unwrap().replace("-", "0").parse().unwrap();
        let deleted: usize = stat_line.next().unwrap().replace("-", "0").parse().unwrap();
        let file = stat_line.next().unwrap();

        let (ref mut t, ref mut a, ref mut d) = *files.entry(file.into()).or_insert((added + deleted, added, deleted));
        *t += added + deleted;
        *a += added;
        *d += deleted;
    }
    files
}
